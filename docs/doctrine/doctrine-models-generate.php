<?php
include_once 'Doctrine/Doctrine.php';

spl_autoload_register(array('Doctrine', 'autoload'));

$manager = Doctrine_Manager::getInstance();

$conn = Doctrine_Manager::connection('mysql://root:root@localhost/square', 'doctrine');

//$databeses = $conn->import->listDatabases();
//print_r($databeses);

Doctrine::generateModelsFromDb(
	'./tmp/models',
	array('doctrine'), 
	array('classPrefix' => 'Square_Model_',
			'classPrefixFiles' => false));
?>